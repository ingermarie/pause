http_path = "/"
css_dir = "css"
sass_dir = "scss"
images_dir = "images"
javascripts_dir = "javascript"
fonts_dir = "fonts"
output_style = :expanded
relative_assets = true
line_comments = false

# To enable relative paths to assets via compass helper functions. Uncomment:
relative_assets = true

Sass::Script::Number.precision = 8