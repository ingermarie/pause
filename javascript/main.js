(function($) {
	var desktopWidth = 960;
	var mobileWidth = 700;

	$(document).ready(readyAndResize);
	$(window).resize(readyAndResize);

	$(document).on('keydown', function(e) {
		if (e.altKey && e.metaKey && String.fromCharCode(e.keyCode) == 'G') {
			$(document.body).children('.grid-vertical, .grid-horizontal').toggle();
		}
	});

	$(document).ready(function(){
		$.localScroll({
			duration: 800,
			hash: true
		});

		// Flexslider
		$('.slideshow').flexslider({
			animation: "fade",
			slideshowSpeed: 5000,
			animationSpeed: 400,
			controlNav: true,
			prevText: "",
			nextText: "",
			// controlsContainer: ".slideshow-nav",
			pauseOnAction: true,
			pauseOnHover: true,
			touch: true,
			keyboard: true
		});
	});

	function readyAndResize() {
		var windowWidth = $(window).width();

		if(windowWidth > mobileWidth) {
			//.parallax(xPosition, speedFactor, outerHeight) options:
			//xPosition - Horizontal position of the element
			//inertia - speed to move relative to vertical scroll. Example: 0.1 is one tenth the speed of scrolling, 2 is twice the speed of scrolling
			//outerHeight (true/false) - Whether or not jQuery should use it's outerHeight option to determine when a section is in the viewport
			$('#intro').parallax("50%", 0.1, true);
			$('#meny').find('.sandwitch-back').parallax("-10%", 0.1, true);
			$('#meny').find('.sandwitch-front').parallax("100%", 0.4, true);
			$('#donut').find('.donut-back').parallax("90%", 0.1, true);
			$('#donut').find('.donut-front').parallax("-10%", 0.4, true);
			// $('#kaffe').parallax("50%", 0.4, true);
			$('#kontakt').parallax("50%", 0.1, true);
		} else {
			$(window).unbind('scroll');
		}
	}
}(jQuery));